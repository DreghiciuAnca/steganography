// OpenCVApplication.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "common.h"
#define CVUI_IMPLEMENTATION
#include "cvui.h"
cv::QtFont;

using namespace std;
using namespace cv;

Mat selectPhoto(char path[]) {
	//char fname[MAX_PATH];
	return imread(path, CV_LOAD_IMAGE_GRAYSCALE);
}

Mat selectGrayScalePhoto() {
	Mat src;
	char fname[MAX_PATH];
	while (openFileDlg(fname)) {
		src = imread(fname, CV_8UC1);
		return src;
	}

	return src;
}

Mat selectPhoto() {
	Mat src;
	char fname[MAX_PATH];
	while (openFileDlg(fname)) {
		src = imread(fname);
		return src;
	}

	return src;
}
void MyCallBackFuncInit(int event, int x, int y, int flags, void* param)
{

	Mat* src = (Mat*)param;
	if (event == CV_EVENT_LBUTTONDOWN)
	{
		Mat frame = Mat(300, 700, CV_8UC3);
		cvui::init("Detalii imagini");
		cvui::printf(frame, 50, 10, 0.4, 0xff0000, "POZITIE PIXELI IMAGINE INITIALA: Pos(%d, %d)", x,y);
		cvui::printf(frame, 50, 20, 0.4, 0xff0000, "VALOARE PIXELI IMAGINE INITIALA: %d", src->at<uchar>(y, x));

		cvui::printf(frame, 370, 10, 0.4, 0xff0000, "VALOARE PIXELI IMAGINE ENCODATA: ");
		cvui::printf(frame, 370, 20, 0.4, 0xff0000, "POZITIE PIXELI IMAGINE ENCODATA: ");
		cvui::imshow("Detalii imagini", frame);

		printf("Pos(x,y): %d,%d  Color(RGB): %d,%d,%d\n",
			x, y,
			(int)(*src).at<Vec3b>(y, x)[2],
			(int)(*src).at<Vec3b>(y, x)[1],
			(int)(*src).at<Vec3b>(y, x)[0]);
	}
}


void MyCallBackFuncDest(int event, int x, int y, int flags, void* param)
{
	Mat* src = (Mat*)param;
	if (event == CV_EVENT_LBUTTONDOWN)
	{
		Mat frame = Mat(300, 700, CV_8UC3);
		cvui::init("Detalii imagini");
		cvui::printf(frame, 50, 10, 0.4, 0xff0000, "POZITIE PIXELI IMAGINE INITIALA: ");
		cvui::printf(frame, 50, 20, 0.4, 0xff0000, "VALOARE PIXELI IMAGINE INITIALA: ");

		cvui::printf(frame, 370, 10, 0.4, 0xff0000, "POZITIE PIXELI IMAGINE ENCODATA: Pos(%d, %d)", x, y);
		cvui::printf(frame, 370, 20, 0.4, 0xff0000, "VALOARE PIXELI IMAGINE ENCODATA: %d", src->at<uchar>(y, x));
		cvui::imshow("Detalii imagini", frame);
		int valoare = src->at<int>(y, x);

		printf("Pos(x,y): %d,%d  Color(RGB): %d,%d,%d\n",
			x, y,
			(int)(*src).at<Vec3b>(y, x)[2],
			(int)(*src).at<Vec3b>(y, x)[1],
			(int)(*src).at<Vec3b>(y, x)[0]);
	}
}
void hideMessage() {
	FILE* out;
	FILE* in;

	in = fopen("file.txt", "r");
	//out = fopen("iesire.txt", "w");

	char cv[100] = " ";
	fgets(cv, 100, in);
	//fputs(cv, out);
	//fclose(out);


	fclose(in);


	char fname[MAX_PATH];
	openFileDlg(fname);

	Mat src = imread(fname, CV_LOAD_IMAGE_COLOR);
	Mat dst = src.clone();

	int index = 0;
	int n = 0;
	n = strlen(cv);
	int height = src.rows;
	int width = src.cols;


	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{


			Vec3b pixel = src.at<Vec3b>(i, j);
			uchar b = pixel[0];
			uchar g = pixel[1];
			uchar r = pixel[2];
			if (index < n)
			{
				byte cuvant = cv[index];

				byte maskb = cuvant & 0xE0;
				byte maskg = cuvant & 0x1C;
				byte maskr = cuvant & 0x03;


				maskb = maskb >> 5;
				maskg = maskg >> 2;
				maskr = maskr;



				b = b & 0xF8 | maskb;
				g = g & 0xF8 | maskg;
				r = r & 0xFC | maskr;
				index += 1;
			}


			Vec3b pixel2(b, g, r);

			dst.at<Vec3b>(i, j) = pixel2;


		}

	}
	namedWindow("Imagine initiala", 1);
	namedWindow("Imagine Encodata", 2);
	Mat frame = Mat(300, 700, CV_8UC3);
	setMouseCallback("Imagine initiala", MyCallBackFuncInit, &src);
	setMouseCallback("Imagine Encodata", MyCallBackFuncDest, &dst);

	//preluare pixel culoare
	Mat dst1 = Mat(height, width, CV_8UC3);
	Mat dst2 = Mat(height, width, CV_8UC3);
	Mat dst3 = Mat(height, width, CV_8UC3);
	Mat dst4 = Mat(height, width, CV_8UC3);
	Mat dst5 = Mat(height, width, CV_8UC3);
	Mat dst6 = Mat(height, width, CV_8UC3);
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			Vec3b pixel = src.at<Vec3b>(i, j);
			Vec3b pixeldest = dst.at<Vec3b>(i, j);
			byte b = pixel[0];
			byte g = pixel[1];
			byte r = pixel[2];

			byte bd = pixeldest[0];
			byte gd = pixeldest[1];
			byte rd = pixeldest[2];
			//printf("uchar: %x", &b);
			byte gnou;
			byte maskag = 0b10000000;
			gnou = g & maskag;
			//printf("gnou: %d", gnou);

			byte gdest;
			byte maskadestg = 0b10000000;
			gdest = maskadestg & gd;
			
			Vec3b bvd(bd, 0, 0), gvd(0, gdest, 0), rvd(0, 0, rd);
			Vec3b bv(b, 0, 0), gv(0, gnou, 0), rv(0, 0, r);

			dst1.at<Vec3b>(i, j) = bv;
			dst2.at<Vec3b>(i, j) = gv;
			dst3.at<Vec3b>(i, j) = rv;
			dst4.at<Vec3b>(i, j) = bvd;
			dst5.at<Vec3b>(i, j) = gvd;
			dst6.at<Vec3b>(i, j) = rvd;

		}
	}
	byte maskag = 0x03;
	printf("byte: %d", maskag);
	
	imshow("output image1", dst1);
	imshow("output image2", dst2);
	imshow("output image3", dst3);
	imshow("output image4", dst4);
	imshow("output image5", dst5);
	imshow("output image6", dst6);

	
	Mat out1(src.rows, src.cols, CV_8UC1, Scalar(0));
	out1 = (src / 2);
	for (int y = 0; y < src.rows; y++) {
		for (int x = 0; x < src.cols; x++)
		{	
			Vec3b pixel = src.at<Vec3b>(y, x);

			byte g = pixel[1];
			byte gfin = g & byte(32);
			
			//byte gfin = gint & byte(2);
			

			Vec3b rout(0, gfin, 0);
			out1.at<Vec3b>(y, x) = rout;
			
		}
	}
	out1 = out1 * 255;

	imshow("out1", out1);

	Mat out2(dst.rows, dst.cols, CV_8UC1, Scalar(0));
	out2 = (dst / 2);
	for (int y = 0; y < dst.rows; y++) {
		for (int x = 0; x < dst.cols; x++)
		{
			Vec3b pixel = dst.at<Vec3b>(y, x);

			byte g = pixel[1];
			byte gfin = g & byte(32);

			//byte gfin = gint & byte(2);


			Vec3b rout(0, gfin, 0);
			out2.at<Vec3b>(y, x) = rout;

		}
	}
	out2 = out2 * 255;

	imshow("out2", out2);
	imshow("Imagine initiala", src);
	imshow("Imagine Encodata", dst);
	imwrite("Imagine_Encodata.bmp", dst);
	waitKey(0);
}

void decodeMessage() {
	char fname[MAX_PATH];
	openFileDlg(fname);

	Mat src = imread(fname, CV_LOAD_IMAGE_COLOR);
	FILE* out;
	out = fopen("iesire.txt", "w");

	char cv[150000] = " ";




	int index = 0;

	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{


			Vec3b pixel = src.at<Vec3b>(i, j);
			uchar b = pixel[0];
			uchar g = pixel[1];
			uchar r = pixel[2];

			byte cuvant = cv[0];
			byte maskb = (b & 0x07) << 5;
			byte maskg = (g & 0x07) << 2;
			byte maskr = r & 0x03;


			cuvant = maskb | maskg | maskr;
			cv[index] = cuvant;
			index++;

			Vec3b pixel2(b, g, r);

		}

	}
	fputs(cv, out);

	fclose(out);


	imshow("Imagine", src);


	waitKey(0);
}
//Sa verific sa vad cat de mult pot sa modific canalele de culoare pt imagine, LSB

void encodeTwo() {
	FILE* out;
	FILE* in;

	in = fopen("file.txt", "r");
	//out = fopen("iesire.txt", "w");

	char cv[100] = " ";
	fgets(cv, 100, in);
	//fputs(cv, out);
	//fclose(out);


	fclose(in);

	char fname[MAX_PATH];
	openFileDlg(fname);

	Mat src = imread(fname, CV_LOAD_IMAGE_COLOR);
	Mat dst = src.clone();

	int index = 0;
	int n = 0;
	n = strlen(cv);



	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{
			if ((i + j) % 2 == 0) // pe pixelul asta se modifica toate canalele
			{

				Vec3b pixel1 = src.at<Vec3b>(i, j);
				uchar b1 = pixel1[0];
				uchar g1 = pixel1[1];
				uchar r1 = pixel1[2];


				if (index < n)
				{
					byte cuvant = cv[index];

					byte maskb = cuvant & 0xC0; // iau de la fiecare canal primii 2 biti
					byte maskg = cuvant & 0x30;
					byte maskr = cuvant & 0x0C;


					maskb = maskb >> 6;
					maskg = maskg >> 4;
					maskr = maskr >> 2;



					b1 = b1 & 0xFC | maskb;
					g1 = g1 & 0xFC | maskg;
					r1 = r1 & 0xFC | maskr;
					
				}


				Vec3b pixelFinal1(b1, g1, r1);

				dst.at<Vec3b>(i, j) = pixelFinal1;
			}
			else // la pixelul asta nu se modifica numai un canal
			{
				Vec3b pixel = src.at<Vec3b>(i, j);
				uchar b = pixel[0];
				uchar g = pixel[1];
				uchar r = pixel[2];

				if (index < n)
				{
					byte cuvant = cv[index];

					byte maskb = cuvant & 0x03; // iau de la cuvant ultimii 2 biti
					byte maskg = cuvant & 0x30;
					byte maskr = cuvant & 0x0C;


					maskb = maskb;


					b = b & 0xFC | maskb;
					
					index += 1;
				}


				Vec3b pixelFinal1(b, g, r);

				dst.at<Vec3b>(i, j) = pixelFinal1;

			}
			


		}

	}

	imshow("Imagine initiala", src);
	imshow("Imagine Encodata", dst);
	imwrite("Imagine_Encodata_2Pixeli.bmp", dst);
	waitKey(0);
}


void decodeTwo() {
	char fname[MAX_PATH];
	openFileDlg(fname);

	Mat src = imread(fname, CV_LOAD_IMAGE_COLOR);
	FILE* out;
	out = fopen("iesireTwo.txt", "w");

	char cv[150000] = " ";


	int index = 0;

	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{
			Vec3b pixel1 = src.at<Vec3b>(i, j);
			uchar b = pixel1[0];
			uchar g = pixel1[1];
			uchar r = pixel1[2];

			byte cuvant = cv[0];
			byte maskb = (b & 0x03) << 6;
			byte maskg = (g & 0x03) << 4;
			byte maskr = (r & 0x03) << 2;

			Vec3b pixel2;
			if ((j + 1) == src.cols)
			{
				int k = i + 1;
				 pixel2 = src.at<Vec3b>(k, 0); // 0 ==j? j o fost inainte
			}
			else {
				int k = j + 1;
				pixel2 = src.at<Vec3b>(i, k);
			}
			uchar b2 = pixel2[0];
			byte mask2b = (b2 & 0x03);
			


			cuvant = maskb | maskg | maskr | mask2b;
			cv[index] = cuvant;
			index++;
			j++;



			Vec3b pixel3(b, g, r);

		}

	}
	fputs(cv, out);

	fclose(out);


	imshow("Imagine", src);


	waitKey(0);
}



void encodeOne() {
	FILE* out;
	FILE* in;

	in = fopen("file.txt", "r");
	//out = fopen("encodare.txt", "w");

	char cv[100] = " ";
	fgets(cv, 100, in);
	//fputs(cv, out);
	//fclose(out);


	fclose(in);


	char fname[MAX_PATH];
	openFileDlg(fname);

	Mat src = imread(fname, CV_LOAD_IMAGE_COLOR);
	Mat dst = src.clone();

	int index = 0;
	int n = 0;
	n = strlen(cv);
	int k = 1;


	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{
			

			if (k == 1) // pe pixelul asta se modifica toate canalele
			{

				Vec3b pixel1 = src.at<Vec3b>(i, j);
				uchar b1 = pixel1[0];
				uchar g1 = pixel1[1];
				uchar r1 = pixel1[2];


				if (index < n)
				{
					byte cuvant = cv[index];

					byte maskb = cuvant & 0x80; // iau de la fiecare canal primul bit
					byte maskg = cuvant & 0x40;
					byte maskr = cuvant & 0x20;


					maskb = maskb >> 7;
					maskg = maskg >> 6;
					maskr = maskr >> 5;



					b1 = b1 & 0xFE | maskb;
					g1 = g1 & 0xFE | maskg;
					r1 = r1 & 0xFE | maskr;

				}


				Vec3b pixelFinal1(b1, g1, r1);

				dst.at<Vec3b>(i, j) = pixelFinal1;
				k = 2;
				continue;
			}
			else if( k == 2) // la pixelul asta nu se modifica numai un canal
			{
				Vec3b pixel = src.at<Vec3b>(i, j);
				uchar b = pixel[0];
				uchar g = pixel[1];
				uchar r = pixel[2];

				if (index < n)
				{
					byte cuvant = cv[index];

					byte maskb = cuvant & 0x10; // iau de la cuvant ultimii 2 biti
					byte maskg = cuvant & 0x08;
					byte maskr = cuvant & 0x04;


					maskb = maskb >> 4;
					maskg = maskg >> 3;
					maskr = maskr >> 2;



					b = b & 0xFE | maskb;
					g = g & 0xFE | maskg;
					r = r & 0xFE | maskr;
					
				}


				Vec3b pixelFinal1(b, g, r);

				dst.at<Vec3b>(i, j) = pixelFinal1;
				k = 3;
				continue;

			}
			else if (k == 3)
			{
				Vec3b pixel = src.at<Vec3b>(i, j);
				uchar b = pixel[0];
				uchar g = pixel[1];
				uchar r = pixel[2];

				if (index < n)
				{
					byte cuvant = cv[index];

					byte maskb = cuvant & 0x02; // iau de la cuvant ultimul bit
					byte maskg = cuvant & 0x01;
					


					maskb = maskb >> 1;
					maskg = maskg;
					



					b = b & 0xFE | maskb;
					g = g & 0xFE | maskg;

					index += 1;

				}


				Vec3b pixelFinal1(b, g, r);

				dst.at<Vec3b>(i, j) = pixelFinal1;

				k = 1;
				continue;
			}



		}

	}

	imshow("Imagine initiala", src);
	imshow("Imagine Encodata", dst);
	imwrite("Imagine_Encodata_3Pixeli.bmp", dst);
	waitKey(0);
}



void decodeOne() {
	char fname[MAX_PATH];
	openFileDlg(fname);

	Mat src = imread(fname, CV_LOAD_IMAGE_COLOR);
	FILE* out;
	out = fopen("iesireOne.txt", "w");

	char cv[150000] = " ";


	int index = 0;

	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{
			Vec3b pixel1 = src.at<Vec3b>(i, j);
			uchar b = pixel1[0];
			uchar g = pixel1[1];
			uchar r = pixel1[2];

			byte cuvant = cv[0];
			byte maskb1 = (b & 0x01) << 7;
			byte maskg1 = (g & 0x01) << 6;
			byte maskr1 = (r & 0x01) << 5;

			Vec3b pixel2;
			Vec3b pixel3;
			if ((j + 1) == src.cols)
			{
				int k = i + 1;
				pixel2 = src.at<Vec3b>(k, j);
				pixel3 = src.at<Vec3b>(k, 1);
			}
			else if ((j + 2) == src.cols)
			{
				int k = i + 1;
				pixel2 = src.at<Vec3b>(i, k);
				pixel3 = src.at<Vec3b>(k, 0);
			}
			else {
				int k = j + 1;
				pixel2 = src.at<Vec3b>(i, k);
				pixel3 = src.at<Vec3b>(i, k + 1);
			}
			uchar b2 = pixel2[0];
			uchar g2 = pixel2[1];
			uchar r2 = pixel2[2];
			byte mask2b = (b2 & 0x01) << 4;
			byte mask2g = (g2 & 0x01) << 3;
			byte mask2r = (r2 & 0x01) << 2;

			uchar b3 = pixel3[0];
			uchar g3 = pixel3[1];
			byte mask3b = (b3 & 0x01) << 1;
			byte mask3g = (g3 & 0x01);



			cuvant = maskb1 | maskg1 | maskr1 | mask2b | mask2g | mask2r | mask3b | mask3g;
			cv[index] = cuvant;
			index++;
			j += 2;



			Vec3b pixel4(b, g, r);

		}

	}
	fputs(cv, out);

	fclose(out);


	imshow("Imagine", src);


	waitKey(0);



}

void dctTransform(uchar matrix[8][8])
{

	int n = 8;
	int m = 8;
	uchar dct_matrix[8][8];

	float ci, cj, dct1, sum;

	for (int i = 0; i < m; i++) {
		for(int j = 0; j < n; j++) {

			
			if (i == 0)
				ci = 1 / sqrt(m);
			else
				ci = sqrt(2) / sqrt(m);
			if (j == 0)
				cj = 1 / sqrt(n);
			else
				cj = sqrt(2) / sqrt(n);


			sum = 0;
			for (int k = 0; k < m; k++) {
				for (int l = 0; l < n; l++) {
					dct1 = matrix[k][l] *
						cos((2 * k + 1) * i * PI / (2 * m)) *
						cos((2 * l + 1) * j * PI / (2 * n));
					sum = sum + dct1;
				}
			}
			dct_matrix[i][j] = ci * cj * sum;
		}
	}

	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++) {
			matrix[i][j] = dct_matrix[i][j];
		}
	}
}


void option1() {
	cout << "Textul care va fi ascuns in imagine se afla in fisierul file.txt.\n";
	//Mat img = selectPhoto();
	hideMessage();
}

void option2() {
	
	decodeMessage();
}

void option3()
{
	encodeTwo();
}



int main()
{
	while (1) {
		system("cls");
		cout << "Alegeti o optiune:\n";
		cout << "1) Ascundeti un text intr-o imagine\n";
		cout << "2) Afisati textul dintr o imagine\n";
		cout << "3) Ascundeti un text intr-o imagine folosind 2 pixeli pentru un caracter\n";
		cout << "4) Afisati textul dintr-o imagine folosind 2 pixeli pentru un caracter\n";
		cout << "5) Ascundeti un text intr-o imagine folosind 3 pixeli pentru un caracter\n";
		cout << "6) Afisati textul dintr-o imagine folosind 3 pixeli pentru un caracter\n";
		cout << "Introduceti un numar: ";
		int option;
		cin >> option;
		switch (option) {
		case 1:
			option1();
			break;
		case 2:
			option2();
			break;
		case 3:
			option3();
			break;
		case 4:
			decodeTwo();
			break;
		case 5:
			encodeOne();
			break;
		case 6: 
			decodeOne();
			break;

		default:
			cout << "Selectati o operatie valida\n";
			break;
		}
		//waitKey();
		system("pause");
	}
	return 0;
}
